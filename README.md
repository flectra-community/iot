# Flectra Community / iot

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[iot_option_oca](iot_option_oca/) | 2.0.1.0.0|         Allow to define custom field for IoT
[iot_output_oca](iot_output_oca/) | 2.0.1.0.0| IoT allow multiple outputs
[iot_input_oca](iot_input_oca/) | 2.0.1.0.0| IoT Input module
[iot_oca](iot_oca/) | 2.0.1.0.0| IoT base module
[iot_template_oca](iot_template_oca/) | 2.0.1.0.1| IoT module for managing templates
[iot_amqp_oca](iot_amqp_oca/) | 2.0.1.0.0| Integrate Iot Outputs with AMQP


